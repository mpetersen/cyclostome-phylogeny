#!/bin/bash

set -euo pipefail

genes_list='doc/genes.txt'

# aggregate and align gene sequences from all samples
cat ${genes_list} | parallel -j 24 bash workflow/scripts/aggregate_genes.sh {} results/040_concatenated results/050_concatenated_aligned results/030_split_fasta/*

# prepare: since the genes list is way too long to concatenate all of them at
# once (400k genes, far too much for a command line), divide and conquer.

# first split the genes list into 1000 parts
split -n 1000 ${genes_list} genes_

# add path info to genes
sed -i -e 's#^#results/050_concatenated_aligned/#' -e 's/$/.mafft.fasta/' genes_*

# concat genes of each part
for f in genes_*; do
    seqkit concat -w 0 $(cat $f) > results/070_supermatrix/supermatrix.fasta.${f}
done

# concat all parts together
seqkit concat -w 0 results/070_supermatrix/supermatrix.fasta.genes_* > results/070_supermatrix/supermatrix.fasta
