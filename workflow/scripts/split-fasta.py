from Bio import SeqIO
from pathlib import Path
import csv
import os
import re
import sys

if "snakemake" in globals():
    input_fasta = str(snakemake.input.fasta)
    output_dir = str(snakemake.output)
    species_name = str(snakemake.wildcards.sample)
else:
    input_fasta = sys.argv[1]
    output_dir = sys.argv[2]
    species_name = sys.argv[3]


output_folder = Path(output_dir)
os.makedirs(output_folder, exist_ok = True)

n_written = 0

for record in SeqIO.parse(input_fasta, "fasta"):
    locus = re.sub("^\w+ ", "", record.description)
    outfile = output_folder / (locus + ".fasta")
    SeqIO.write(record, outfile, "fasta")
    n_written += 1

print("Wrote " + str(n_written) + " records")
