#!/bin/bash

set -euo pipefail

if [[ $# -lt 4 ]]; then
    echo "usage: $0 gene concat_dir align_dir dir [dir dir dir ...]"
    exit 1
fi

# assign arguments
args=( "$@" )
gene=${args[0]}
concat_dir=${args[1]}
align_dir=${args[2]}
dirs=${args[@]:3}

echo "Creating output directories"
log_dir="logs/mafft"

mkdir -p ${concat_dir}
mkdir -p ${align_dir}
mkdir -p ${log_dir}


printf "Aggregating sequences for $gene\n"

# make sure all fasta files exist. Not just touch them, but add an empty
# sequence so that they will also end up in the alignments.
for dir in ${dirs[@]}; do
    #echo "Checking ${dir}/${gene}.fasta"
    sp=$(basename "${dir}")
    fafile="${dir}/${gene}.fasta"
    if [[ ! -s "$fafile" ]]; then
        echo "$fafile empty, adding empty seq"
        printf ">%s %s\n\n" "${sp}" "${gene}" > "$fafile"
    fi
done

# concatenate files
for dir in ${dirs[@]}; do
    #echo "cat'ing ${dir}/${gene}.fasta" >&2
    cat "${dir}/${gene}.fasta"
done > "${concat_dir}/${gene}.fasta"

# align
mafft --amino "${concat_dir}/${gene}.fasta" 2> "${log_dir}/${gene}.mafft.log" \
    | seqkit replace --line-width 0 --pattern ' .+' --replacement '' \
    > "${align_dir}/${gene}.mafft.fasta"
