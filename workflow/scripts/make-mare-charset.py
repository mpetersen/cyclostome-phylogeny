#!/usr/bin/env python3

import sys
import csv
import re

if len(sys.argv) != 2:
    sys.exit("Usage: {cmd} stats_file".format(cmd = sys.argv[0]))

partition = 0
part_start = 1
part_end = 0

with open(sys.argv[1], "r") as fh:
    rows = csv.reader(fh, delimiter = "\t")
    # read line, extract name and length
    for row in rows:
        if row[0] == "file": continue
        gene = row[0].removesuffix(".mafft.fasta")
        # add up lengths
        partition = partition + 1
        part_start = part_end + 1
        part_end = part_start + int(row[-1]) - 1
        print("charset p{partition} = {start} - {end} ;".format(partition = str(partition).zfill(6), start = str(part_start), end = str(part_end)))
