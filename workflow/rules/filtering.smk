import os
import re


datadir = "data/raw"

# read the sample sheet
read_files = { "R1": dict(), "R2": dict() } # Keys: sample names, Values: list of read1 files
for sample in samples["sample"]:
    if sample not in read_files["R1"]:
        read_files["R1"][sample] = []
        read_files["R2"][sample] = []
    fq1 = units.loc[sample, "fq1"]
    fq2 = units.loc[sample, "fq2"]
    if isinstance(fq1, pd.Series): # multiple files
        fq1 = fq1.to_list()
        fq2 = fq2.to_list()
        read_files["R1"][sample].extend(fq1)
        read_files["R2"][sample].extend(fq2)
    else: # single file
        read_files["R1"][sample].append(fq1)
        read_files["R2"][sample].append(fq2)

# If there's no merging, then just make a symlink
for sampleName, files in read_files["R1"].items():
    os.makedirs(datadir, exist_ok=True)
    if len(files) == 1 and not os.path.exists(os.path.join(datadir, "{}_R1.fastq.gz".format(sampleName))):
        os.symlink(files[0], os.path.join(datadir, "{}_R1.fastq.gz".format(sampleName)))
for sampleName, files in read_files["R2"].items():
    if len(files) == 1 and not os.path.exists(os.path.join(datadir, "{}_R2.fastq.gz".format(sampleName))):
        os.symlink(files[0], os.path.join(datadir, "{}_R2.fastq.gz".format(sampleName)))



def link_data(sample_name, file_path):
    link_path = os.path.join("data/raw", "{sample_name}_R1.fastq.gz".format(sample_name = sample_name))
    if not os.path.exists(link_path):
        print("Symlinking {source} to {destination}".format(source = file_path, destination = link_path))
        os.symlink(file_path, link_path)
    else:
        print("{path} already exists".format(path = link_path))
    file_path2 = re.sub("_R1\.fastq\.gz$", "_R2.fastq.gz", file_path)
    link_path2 = re.sub("_R1\.fastq\.gz$", "_R2.fastq.gz", link_path)
    if not os.path.exists(link_path2):
        print("Symlinking {source} to {destination}".format(source = file_path2, destination = link_path2))
        os.symlink(file_path2, link_path2)
    else:
        print("{path} already exists".format(path = link_path2))

localrules: mergeFQ
rule mergeFQ:
    input:
        lambda wildcards: read_files[wildcards.read][wildcards.sample]
    output:
        "data/raw/{sample}_{read}.fastq.gz"
    shell:
        """
        cat {input} > {output}
        """


rule fastp_pe:
    input:
        sample=["data/raw/{sample}_R1.fastq.gz", "data/raw/{sample}_R2.fastq.gz"]
    output:
        trimmed=["data/clean/{sample}_R1.fastq.gz", "data/clean/{sample}_R2.fastq.gz"],
        html="report/fastp/{sample}.html",
        json="report/fastp/{sample}.json"
    log:
        "logs/fastp/pe/{sample}.log"
    benchmark:
        "benchmarks/fastp/pe/{sample}.fastp.benchmark.txt"
    params:
        adapters="",
        extra=""
    threads: 12
    wrapper:
        "0.72.0/bio/fastp"
