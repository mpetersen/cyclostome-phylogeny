localrules: report
rule report:
    input: "results/tables/length-vs-ns.tsv.gz"
    output: "report/report.html"
    conda: "../envs/knitr.yaml"
    params:
        figures_dir = "results/figures",
        tables_dir = "results/tables"
    script: "../../" + config["report_rmd"]
