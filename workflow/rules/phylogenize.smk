localrules: translate
rule translate:
    input:
        fasta = "results/010_exfi/{sample}/{sample}.gapped.renamed.fasta"
    output:
        fasta = "results/020_translated/{sample}.gapped.renamed.aa.fasta"
    conda:
        "../envs/seqkit.yaml"
    params:
        "--allow-unknown-codon --frame 1"
    shell:
        """
        seqkit translate {params} {input} > {output}
        """

localrules: split_fasta
rule split_fasta:
    input:
        fasta = "results/020_translated/{sample}.gapped.renamed.aa.fasta"
    output:
        directory("results/030_split_fasta/{sample}")
    conda:
        "../envs/biopython.yaml"
    script:
        "../scripts/split-fasta.py"

rule aggregate_and_align:
    input:
        genes_list = "doc/genes.txt",
        directories = expand("results/030_split_fasta/{sample}", sample = samples["sample"])
    output:
        concat_dir = directory("results/040_concatenated"),
        align_dir = directory("results/050_concatenated_aligned"),
        flag_file = "results/050_concatenated_aligned/all_present"
    threads: 48
    conda:
        "../envs/alignment.yaml"
    shell:
        """
        cat {input.genes_list} | parallel -j {threads} workflow/scripts/aggregate_genes.sh {{}} {output.concat_dir} {output.align_dir} {input.directories}
        touch {output.flag_file}
        """
