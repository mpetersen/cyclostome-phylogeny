localrules: download_pfam
rule download_pfam:
    output:
        "data/ref/Pfam-A.hmm.gz"
    shell:
        "wget -O {output} http://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/Pfam-A.hmm.gz"

localrules: install_sat_assembler
rule install_sat_assembler:
    output:
        "code/SAT-Assembler/SAT-Assembler.sh"
    conda:
        # TODO
    shell:
        "git clone https://github.com/jiarong/SAT-Assembler.git code/SAT-Assembler"

localrules: fq2fa
rule fq2fa:
    input:
        "data/clean/{sample}_R1.fastq.gz"
    output:
        "data/clean/{sample}_R1.fasta"
    envmodules:
        "seqtk"
    shell:
        "seqtk seq -A {input} > {output}"

rule SAT_assembler:
    input:
        fasta = "data/clean/{sample}.fasta",
        assembler = "code/SAT-Assembler/SAT-Assembler.sh",
        hmmdb = "data/ref/Pfam-A.hmm.gz"
