from snakemake.utils import validate
import pandas as pd
import sys

# this container defines the underlying OS for each job when using the workflow
# with --use-conda --use-singularity
#singularity: "docker://continuumio/miniconda3"

##### load config and sample sheets #####

configfile: "config/config.yaml"
validate(config, schema="../schemas/config.schema.yaml")

units = pd.read_csv(config["units"], sep="\t").set_index("sample", drop=False)
validate(units, schema="../schemas/units.schema.yaml")

samples = pd.read_csv(config["samples"], sep="\t").set_index("sample", drop=False)

def is_single_end(sample, unit):
    """Determine whether unit is single-end."""
    return pd.isnull(units.loc[(sample, unit), "fq2"])

def get_fastqs(wildcards):
    """Get raw FASTQ files from unit sheet."""
    if is_single_end(wildcards.sample, wildcards.unit):
        return units.loc[ (wildcards.sample, wildcards.unit), "fq1" ]
    else:
        u = units.loc[ (wildcards.sample), ["fq1", "fq2"] ].dropna()
        return { "fq1": f"{u.fq1}", "fq2": f"{u.fq2}" }

def get_fq_for_species(wildcards):
    s = units.loc[wildcards.shorthand, [ "fq1", "fq2" ] ].dropna()
    fq1 = s["fq1"].tolist()
    fq2 = s["fq2"].tolist()
    return { "fq1": fq1, "fq2": fq2 }


rule raw_data_stats:
    input:
        expand("data/raw/{sample}_R{group}.merged.fastq.gz", sample = units["sample"], group = [1, 2])
    output:
        "report/raw-data.stats"
    conda:
        "../envs/seqkit.yaml"
    params:
        "-b"
    shell:
        "seqkit stats {params} {input} > {output}"

localrules: gene_list
rule gene_list:
    input:
        config["genes_fasta"]
    output:
        "doc/genes.txt"
    conda:
        "../envs/seqkit.yaml"
    shell:
        """
        seqkit fx2tab -n {input} | sed -e 's/ .\+$//' > {output}
        """
