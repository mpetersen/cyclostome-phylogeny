localrules: bwa_index
rule bwa_index:
    input:
        config["genes_fasta"]
    output:
        str(config["genes_fasta"]) + ".bwt"
    envmodules:
        "bwa-mem2"
    shell:
        "bwa-mem2 index {input}"

rule map_bwa:
    input:
        get_trimmed
    output:
        "results/mapping/bwa/{sample}.bam"
    envmodules:
        "bwa-mem2",
        "samtools"
    threads:
        12
    params:
        ref_index = config["genes_fasta"]
    shell:
        "bwa-mem2 mem -t {threads} {params.ref_index} {input} | samtools view -hb > {output}"

localrules: mapping_flagstats
rule mapping_flagstats:
    input:
        "results/mapping/bwa/{sample}.bam"
    output:
        "results/mapping/bwa/{sample}.bam.stats"
    envmodules:
        "samtools"
    shell:
        "samtools flagstat {input} > {output}"

localrules: filter_unmapped
rule filter_unmapped:
    input:
        "results/mapping/bwa/{sample}.bam"
    output:
        "results/mapping/bwa/{sample}.filtered.bam"
    envmodules:
        "samtools"
    shell:
        "samtools view -hb -F 4 -o {output} {input}"
