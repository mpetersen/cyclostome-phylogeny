localrules: install_exfi
rule install_exfi:
    output:
        "results/010_exfi/exfi.installed"
    conda:
        "../envs/exfi.yaml"
    shell:
        """
        pip install https://github.com/jlanga/exfi/archive/v1.5.6.zip
        touch {output}
        """

localrules: install_biobloomtools
rule install_biobloomtools:
    output:
        biobloommaker = config["biobloommaker"],
        biobloomcategorizer = config["biobloomcategorizer"]
    log:
        "logs/install_biobloomtools.log"
    conda:
        "../envs/compile-biobloom.yaml"
    params:
        threads = 4
    shell:
        """
        rm -rf biobloom
        git clone --recursive https://github.com/bcgsc/biobloom.git > {log}
        cd biobloom/
        git submodule update --init >> {log}
        git checkout 0a42916922d42611a087d4df871e424a8907896e >> {log}
        ./autogen.sh >> {log}
        ./configure --prefix=\"$(realpath ../code)\" >> {log}
        make -j {params.threads} >> {log}
        make install >> {log}
        """

def get_trimmed(wildcards):
    return [ "data/clean/{sample}_R1.fastq.gz".format(sample = wildcards.sample),
             "data/clean/{sample}_R2.fastq.gz".format(sample = wildcards.sample) ]

rule exfi_build:
    input:
        fq = get_trimmed,
        exfi = "results/010_exfi/exfi.installed",
        biobloommaker = config["biobloommaker"]
    output:
        bloom = "results/010_exfi/{sample}/{sample}.bf"
    log:
        "logs/exfi/{sample}.exfi_1_build.log"
    benchmark:
        "benchmarks/exfi/{sample}.exfi_1_build.benchmark.txt"
    params:
        transcriptome = config["genes_fasta"],
        kmer_size     = config["params"]["kmer_size"],
        bloom_size    = config["params"]["bloom_size"],
        bloom_levels  = config["params"]["bloom_levels"]
    threads:
        32
    conda:
        "../envs/exfi.yaml"
    shell:
        """
        echo '# Building Bloom filter' | tee {log}
        export PATH="$PATH:$(dirname {input.biobloommaker})"
        build_baited_bloom_filter \
        --threads {threads} \
        --input-fasta {params.transcriptome} \
        --kmer {params.kmer_size} \
        --bloom-size {params.bloom_size} \
        --levels {params.bloom_levels} \
        --output-bloom {output.bloom} \
        --verbose \
        {input.fq[0]} {input.fq[1]} 2>> {log}
        """

rule exfi_splice_graph:
    input:
        exfi = "results/010_exfi/exfi.installed",
        bloom = "results/010_exfi/{sample}/{sample}.bf",
        biobloommaker = "code/bin/biobloommaker"
    output:
        gfa = "results/010_exfi/{sample}/{sample}.gfa"
    log:
        "logs/exfi/{sample}.exfi_2_splice_graph.log"
    benchmark:
        "benchmarks/exfi/{sample}.exfi_2_splice_graph.benchmark.txt"
    params:
        transcriptome = config["genes_fasta"],
        kmer_size = config["params"]["kmer_size"],
        max_consecutive_fp = config["params"]["max_consecutive_fp"]
    threads:
        8
    conda:
        "../envs/exfi.yaml"
    shell:
        """
        echo '# Building splice graph' | tee -a {log}
        export PATH="$PATH:$(dirname {input.biobloommaker})"
        build_splice_graph \
        --threads {threads} \
        --input-fasta {params.transcriptome} \
        --input-bloom {input.bloom} \
        --kmer {params.kmer_size}\
        --max-fp-bases {params.max_consecutive_fp} \
        --output-gfa {output.gfa} \
        --verbose \
        2>> {log}
        """

localrules: exfi_gfa2fasta
rule exfi_gfa2fasta:
    input:
        exfi = "results/010_exfi/exfi.installed",
        gfa = "results/010_exfi/{sample}/{sample}.gfa"
    output:
        gapped_fasta = "results/010_exfi/{sample}/{sample}.gapped.fasta"
    log:
        "logs/exfi/{sample}.exfi_3_gfa2fasta.log"
    benchmark:
        "benchmarks/exfi/{sample}.exfi_3_gfa2fasta.benchmark.txt"
    params:
        num_n_between_exons = config["params"]["num_n_between_exons"]
    threads:
        1
    conda:
        "../envs/exfi.yaml"
    shell:
        """
        echo '# Converting GFA to gapped Fasta' | tee -a {log}
        gfa1_to_fasta \
        --input-gfa {input.gfa} \
        --output-fasta {output.gapped_fasta} \
        --gapped-transcript \
        --number-of-ns {params.num_n_between_exons} \
        --verbose \
        2>> {log}
        echo '# All done.' | tee -a {log}
        """

localrules: tabulate_sequence_stats
rule tabulate_sequence_stats:
    input:
        expand("results/010_exfi/{sample}/{sample}.gapped.renamed.fasta", sample = samples["sample"])
    output:
        "results/tables/length-vs-ns.tsv.gz"
    conda:
        "../envs/seqkit.yaml"
    shell:
        """
        ( printf "%s\t%s\t%s\t%s\n" 'sample' 'name' 'length' 'perc_N'
        for f in {input}; do
            seqkit fx2tab --name --length --base-content N $f | sed -e "s/^/$(basename $f .gapped.renamed.fasta)\t/"
        done ) \
        | gzip \
        > {output}
        """

localrules: rewrite_headers
rule rewrite_headers:
    input:
        fasta = "results/010_exfi/{sample}/{sample}.gapped.fasta"
    output:
        fasta = "results/010_exfi/{sample}/{sample}.gapped.renamed.fasta",
        dictionary = "results/010_exfi/{sample}/{sample}.gapped.renamed.fasta.dict"
    conda:
        "../envs/seqkit.yaml"
    shell:
        """
        sed -e '/^>/s/ .\+//' -e 's/^>/>{wildcards.sample} /' {input.fasta} > {output.fasta}
        seqkit fx2tab -n {input.fasta} > {output.dictionary}
        """
