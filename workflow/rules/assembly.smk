# make sure that BioBloomTools are in PATH
from shutil import which
if which("biobloomcategorizer") is None:
    print("biobloomcategorizer not found, is it in PATH?")
    sys.exit(1)


def get_fq_for_species(wildcards):
    s = samples.loc[wildcards.shorthand, [ "fq1", "fq2" ] ].dropna()
    fq1 = s["fq1"].tolist()
    fq2 = s["fq2"].tolist()
    return { "fq1": fq1, "fq2": fq2 }

def get_fastqs(wildcards):
    """Get raw FASTQ files from unit sheet."""
    if is_single_end(wildcards.sample, wildcards.unit):
        return units.loc[ (wildcards.sample, wildcards.unit), "fq1" ]
    else:
        u = units.loc[ (wildcards.sample), ["fq1", "fq2"] ].dropna()
        return [ f"{u.fq1}", f"{u.fq2}" ]

def get_trimmed(wildcards):
    return [ "data/clean/{sample}_R1.fastq.gz".format(sample = wildcards.sample), "data/clean/{sample}_R2.fastq.gz".format(sample = wildcards.sample) ]

rule assemble_spades:
    input:
        r1 = "data/clean/{sample}_R1.fastq.gz",
        r2 = "data/clean/{sample}_R2.fastq.gz"
    output: "results/assembly/spades/{sample}/contigs.fasta"
    log: "logs/spades/{sample}.spades.log"
    benchmark: "benchmarks/spades/{sample}.spades.benchmark.txt"
    conda: "../envs/spades.yaml"
    threads: 48
    params:
        checkpoints = "last",
        cov_cutoff = "auto",
        max_mem = 750,
        tmp_dir = "scratch/spades_tmp/{sample}"
    shell:
        """
        spades.py --isolate \
            --threads {threads} \
            --memory {params.max_mem} \
            --tmp-dir {params.tmp_dir} \
            -o $(dirname {output}) \
            -1 {input.r1} \
            -2 {input.r2} \
            --checkpoints {params.checkpoints} \
            --cov-cutoff {params.cov_cutoff} \
            > {log}
        """

rule metaspades:
    input:
        r1 = "data/clean/{sample}_R1.fastq.gz",
        r2 = "data/clean/{sample}_R2.fastq.gz"
    output: "results/assembly/metaspades/{sample}/contigs.fasta"
    log: "logs/metaspades/{sample}.metaspades.log"
    benchmark: "benchmarks/metaspades/{sample}.metaspades.benchmark.txt"
    conda: "../envs/spades.yaml"
    threads: 48
    params:
        checkpoints = "last",
        cov_cutoff = "auto",
        max_mem = 750,
        tmp_dir = "scratch/spades_tmp/{sample}",
        hmmdir = config["hmmdir"]
    shell:
        """
        metaspades.py \
            --threads {threads} \
            --memory {params.max_mem} \
            --tmp-dir {params.tmp_dir} \
            -o $(dirname {output}) \
            -1 {input.r1} \
            -2 {input.r2} \
            --checkpoints {params.checkpoints} \
            --custom-hmms {params.hmmdir} \
            > {log}
        """
