# Cyclostome phylogeny

Infer a phylogenetic tree of cyclostomes (lamprey and hagfish) from
whole genome sequencing data.

Envisioned workflow:

1. [EXFI](https://github.com/jlanga/exfi) with the transcriptome
   assembly of one *Lampetra planeri* sample as a reference. This
   exon prediction approach uses cascading Bloom filters to pool and
   assemble reads that match the transcriptome.
2. [MAFFT](https://mafft.cbrc.jp/alignment/software/) to align each exon
   group.
2a. Retain exons with at least 90% of samples (48) and at least 30 aa
3. [Seqkit](https://bioinf.shenwei.me/seqkit/) concat to create a
   concatenated data matrix out of the alignment blocks.
4. [TrimAl](http://trimal.cgenomics.org/) to trim the concatenated
   alignment of low-quality columns in order to increase phylogenetic
   information content.
5. [MARE](http://mare.zfmk.de) to optimise the supermatrix toward
   maximum phylogenetic information content.
6. [IQTree](http://www.iqtree.org/) (ML) or
   [PhyloBayes](http://www.atgc-montpellier.fr/phylobayes/) (BI) to
   infer the phylogeny.

So far, we have processed these samples:

| sample            | longer_than | sequences |
|-------------------|-------------|-----------|
| G_australis_1     | 300         | 684       |
| G_australis_2     | 300         | 776       |
| G_australis_3     | 300         | 741       |
| G_australis_4     | 300         | 701       |
| G_australis_5     | 300         | 735       |
| Lp236_1           | 300         | 260529    |
| M_mordax_Murray_1 | 300         | 3510      |
| M_mordax_Murray_2 | 300         | 3694      |
| M_mordax_Murray_3 | 300         | 3158      |

Table generated with [Tables Generator](https://www.tablesgenerator.com/markdown_tables) from results/tables/sequences_longer_than_300_nt.tsv

# BioBloomTools installation

This is the most awful manual step in the setup. Set up a Conda
environment for compilation:

```
conda install -c conda-forge boost google-sparsehash sdsl-lite zlib
```

Then, follow the [instructions from the EXFI
documentation](https://github.com/jlanga/exfi#how-to-install):

```
git clone --recursive https://github.com/bcgsc/biobloom.git
cd biobloom
git submodule update --init
git checkout 0a42916922d42611a087d4df871e424a8907896e
./autogen.sh
# set the prefix to a location where the pipeline finds the binaries
# (see config/config.yaml)
./configure --prefix=/data/processing5/petersen/projects/cyclostome-phylogeny/code/bin
make -j 4
make install
```

I want to implement the Biobloomtools installation in a separate rule.
